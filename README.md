## Code Status
[![Build Status](https://travis-ci.org/Shkrt/flashcards.svg?branch=travisci)](https://travis-ci.org/Shkrt/flashcards)
[![Code Climate](https://codeclimate.com/github/Shkrt/flashcards/badges/gpa.svg)](https://codeclimate.com/github/Shkrt/flashcards)
[![Test Coverage](https://codeclimate.com/github/Shkrt/flashcards/badges/coverage.svg)](https://codeclimate.com/github/Shkrt/flashcards/coverage)

http://floating-stream-6912.herokuapp.com/

describe 'CardReviewUpdater' do
  it 'sets quality to 0 when incorrect answer given' do
    c = CardReviewUpdater.new(14, false, 2.5)
    expect(c.quality).to eq 0
  end

  it 'grades quality according to answer time, when correct answer was given' do
    arr = []
    (0..12).step(2) { |x| arr << CardReviewUpdater.new(x, true, 2.5) }
    expect(arr[0].quality).to eq 5
    expect(arr[1].quality).to eq 4
    expect(arr[4].quality).to eq 3
    expect(arr[5].quality).to eq 2
    expect(arr[6].quality).to eq 1
  end
end

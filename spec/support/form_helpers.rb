module FormHelpers
  def register(e_mail, pass, confirmation)
    :available
    visit (new_user_path)
    fill_in :user_email, with: e_mail
    fill_in :user_password, with: pass
    fill_in :user_password_confirmation, with: confirmation
    click_button I18n.t('save')
  end

  def sign_in(e_mail, pass)
    :available
    visit (login_path)
    fill_in :session_email, with: e_mail
    fill_in :session_password, with: pass
    click_button I18n.t('nav.signin')
  end
end

describe 'Creating a deck' do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }
  let!(:deck) { create(:deck, user: user) }
  let!(:card) { create(:card, user: user, deck: deck) }

  before(:each) do
    sign_in(user.email, 'qwerty')
  end

  it 'shows warning when deck name is not provided' do
    visit (new_deck_path)
    click_button I18n.t('save')
    expect(page).to have_content('can\'t be blank')
  end

  it 'shows decks\' index when new deck is created' do
    deck_name = 'test_name'
    visit (new_deck_path)
    fill_in :deck_name, with: deck_name
    click_button I18n.t('save')
    expect(page).to have_content(deck_name)
  end
end

describe 'Logging in' do
  let!(:user) { create :user }

  it 'passes successfully when correct credentials are given' do
    sign_in(user.email, 'qwerty')
    expect(page).to have_content I18n.t('flash.signin')
  end

  it 'fails when incorrect credentials are given' do
    sign_in('12@ut.ui', 'qoipo')
    expect(page).to have_content I18n.t('flash.unable_sign')
  end

  it 'fails when password field is empty' do
    sign_in('12@ut.ui', '')
    expect(page).to have_content I18n.t('flash.unable_sign')
  end

  it 'fails when email field is empty' do
    sign_in('', 'posswurd')
    expect(page).to have_content I18n.t('flash.unable_sign')
  end
end

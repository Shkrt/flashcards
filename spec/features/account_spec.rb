require 'rails_helper'
context 'Non-authenticated user' do
  it "doesn't see 'all cards' link" do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.cards')
  end

  it "doesn't see 'new card' link" do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.add_card')
  end

  it "doesn't see 'logout' link" do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.signout')
  end

  it "doesn't see 'account' link" do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.account')
  end

  it 'sees google external login link' do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.google')
  end

  it 'sees login link' do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.signin')
  end

  it 'doesn\'t see decks link' do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.decks')
  end

  it 'sees change language links' do
    visit (root_path)
    expect(page).to have_selector ('a[href*="locale"]')
  end
end

context 'Authenticated user' do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }
  let!(:deck) { create(:deck, user: user) }
  let!(:card) { create(:card, user: user, deck: deck) }

  before(:each) do
    sign_in(user.email, password = 'qwerty')
  end

  it "sees 'all cards' link" do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.cards')
  end

  it "sees 'new card' link" do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.add_card')
  end

  it "sees 'account' link" do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.account')
  end

  it "sees 'logout' link" do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.signout')
  end

  it "doesn't see google external login link" do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.google')
  end

  it "doesn't see login link" do
    visit (root_path)
    expect(page).not_to have_content I18n.t('nav.signin')
  end

  it 'sees edit account link' do
    visit (users_path)
    expect(page).to have_content I18n.t('dashboard.users.index.edit_account')
  end

  it 'sees decks link' do
    visit (root_path)
    expect(page).to have_content I18n.t('nav.decks')
  end

  it 'doesn\'t see change language links' do
    visit (root_path)
    expect(page).not_to have_selector ('a[href*="locale"]')
  end
end

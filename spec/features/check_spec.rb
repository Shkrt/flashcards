require 'rails_helper'
def enter(data)
  visit (root_path)
  fill_in :card_user_entered, with: data
  click_button I18n.t('dashboard.cards.index.check')
end

describe 'Checking a card', :js => true do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }
  let!(:deck) { create(:deck, user: user) }
  let!(:card) { create(:card, user: user, deck: deck, review_date: (-2).days.from_now) }

  before(:each) do
    sign_in(user.email, 'qwerty')
  end

  it 'shows warning when answer is incorrect' do
    enter('fail')
    expect(page).to have_content I18n.t('flash.answer_wrong')
  end

  it 'shows ok when answer is correct' do
    enter('success')
    expect(page).to have_content I18n.t('flash.answer_correct')
  end

  it "shows typo warning in case of mistyping 2 letters" do
    enter("suddess")
    expect(page).to have_content I18n.t('flash.mistyped')
  end

  it "doesn\'t show typo warning in case of mistyping more than 2 letters" do
    enter("suddoss")
    expect(page).not_to have_content I18n.t('flash.mistyped')
  end

end

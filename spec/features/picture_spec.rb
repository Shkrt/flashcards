describe 'Attachment' do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }
  let!(:deck) { create(:deck, user: user) }
  let!(:card) { create(:card, user: user, review_date: (-2).days.from_now,
       picture:  fixture_file_upload('test_img.png', 'image/png'), deck: deck) }

  before(:each) do
    sign_in(user.email, password = 'qwerty')
  end

  it 'is shown when user checks a card' do
    visit (root_path)
    expect(page).to have_selector ('div.form-group > img')
  end

  it 'is shown when user edits a card' do
    visit (edit_card_path(card))
    expect(page).to have_selector ('div.form-group > img')
  end

  it 'is shown in cards\' index' do
    visit (cards_admin_path)
    expect(page).not_to have_selector ('img[alt="Missing"]')
  end

  it 'has removal button' do
    visit (edit_card_path(card))
    expect(page).to have_content I18n.t('card.remove_image')
  end
end

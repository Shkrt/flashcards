describe 'Registration' do
  let!(:user) { create :user }

  it 'passes successfully when correct credentials are given' do
    register(:email, 'qwerty', 'qwerty')
    expect(page).to have_content I18n.t('nav.account')
  end

  it 'fails when password confirmation does not match password' do
    register(:email, 'qoipo', 'wer')
    expect(page).to have_content 'doesn\'t match Password'
  end

  it 'fails when password is too short' do
    register(:email, '11', '11')
    expect(page).to have_content 'is too short'
  end

  it 'fails when password field is empty' do
    register(:email, '', '111')
    expect(page).to have_content 'can\'t be blank'
  end

  it 'fails when email field is empty' do
    register('', '111', '111')
    expect(page).to have_content 'can\'t be blank'
  end
end

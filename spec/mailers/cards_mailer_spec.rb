require "rails_helper"

describe CardsMailer, type: :mailer do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }
  let!(:mail) { CardsMailer.pending_card_notifications(user) }

  it "renders the receiver email" do
    expect(mail.to).to eq([user.email])
  end

  it "renders the sender email" do
    expect(mail.from).to eq([Rails.application.config.action_mailer.smtp_settings[:user_name]])
  end
end

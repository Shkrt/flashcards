require "codeclimate-test-reporter"
CodeClimate::TestReporter.start
require File.expand_path('../../config/environment', __FILE__)
require 'paperclip/matchers'
require 'capybara/poltergeist'
require 'rspec/rails'
require 'capybara/rspec'
require 'capybara/rails'

Capybara.javascript_driver = :poltergeist
Capybara.default_max_wait_time = 5
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  config.include Sorcery::TestHelpers::Rails::Controller, type: :controller
  config.include Sorcery::TestHelpers::Rails::Integration, type: :feature
  config.include Paperclip::Shoulda::Matchers
  config.include ActionDispatch::TestProcess
  config.include FactoryGirl::Syntax::Methods

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.before(:each) do
    if Capybara.current_driver == :poltergeist
      page.driver.headers = { 'Accept-Language' => "en" }
    end
  end
end

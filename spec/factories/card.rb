FactoryGirl.define do
  sequence :translated_text do |n|
    "some_text_#{n}"
  end

  factory :card do
    original_text 'success'
  end
end

FactoryGirl.modify do
  factory :card do
    original_text 'success'
    translated_text {generate(:translated_text)}
    review_date (-1).days.from_now
  end
end

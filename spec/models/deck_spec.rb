require 'rails_helper'

describe Deck, type: :model do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }

  it 'is not valid without a name' do
    deck = build(:deck, name: '', user: user)
    expect(deck).to be_invalid
  end
end

require 'rails_helper'

describe Card, type: :model do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }
  let!(:deck) { create(:deck, user: user) }

  it 'includes expired cards into review_expired scope' do
    card = create(:card, original_text: 'Text', translated_text: 'Текст', review_date: (-2).days.from_now, user: user, deck: deck)
    cards = Card.all
    expect(cards.review_expired).to include(card)
  end

  it 'does not include non-expired cards into review_expired scope' do
    card = create(:card, original_text: 'Try', translated_text: 'Test', review_date: 3.days.from_now, user: user, deck: deck)
    expect(Card.all.review_expired).not_to include(card)
  end

  it '.check_translation returns true when translation is correct' do
    card = create(:card, original_text: 'success', translated_text: 'e', review_date: 3.days.from_now, user: user, deck: deck)
    expect(card.check_translation("success")[:correct]).to be true
  end

  it '.check_translation returns true when only difference is letters case' do
    card = create(:card, original_text: 'success', translated_text: 'e', review_date: 3.days.from_now, user: user, deck: deck)
    expect(card.check_translation("SuCcEsS")[:correct]).to be true
  end

  it '.check_translation returns false when translation is incorrect' do
    card = create(:card, original_text: 'success', translated_text: 'e', review_date: 3.days.from_now, user: user, deck: deck)
    expect(card.check_translation("You Phail")[:correct]).to be false
  end

  it ".check_translation returns message when translation contains no more than 2 mistypings" do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck)
    expect(card.check_translation("suddess")[:message]).to be_instance_of(String)
  end

  it ".check_translation doesn\'t return message when translation contains more than 2 mistypings" do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck)
    expect(card.check_translation("suddoss")[:message]).to eq(nil)
  end

  it 'validates all fields' do
    card = build(:card, original_text: 'Try', translated_text: 'Try', review_date: 3.days.from_now, user: user, deck: deck)
    card_1 = build(:card, original_text: 'Try', translated_text: 'Test', review_date: 3.days.from_now, user: user, deck: deck)
    card_2 = build(:card, original_text: 'Try', translated_text: 'tRy', review_date: 3.days.from_now, user: user, deck: deck)
    card_3 = build(:card, original_text: '', translated_text: 'Text', review_date: 3.days.from_now, user: user, deck: deck)
    card_4 = build(:card, original_text: 'Text', translated_text: '', review_date: 3.days.from_now, user: user, deck: deck)
    card_5 = build(:card, original_text: 'Text', translated_text: 'Текст', review_date: nil, user: user, deck: deck)
    card_6 = build(:card, original_text: 'Text', translated_text: 'Текст', review_date: (-2).days.from_now, user: user, deck: nil)
    expect(card).to be_invalid
    expect(card_1).to be_valid
    expect(card_2).to be_invalid
    expect(card_3).to be_invalid
    expect(card_4).to be_invalid
    expect(card_5).to be_invalid
    expect(card_6).to be_invalid
  end

  it { should validate_attachment_content_type(:picture) }

  it 'schedules card for later review if answer quality is below 4' do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck, repetition_total: 12)
    card.manage_review_dates(14, true)
    expect(card.repeat_later).to be true
  end

  it 'does not schedule card for later review if answer quality is above 4' do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck, repetition_total: 12)
    card.manage_review_dates(0, true)
    expect(card.repeat_later).not_to be true
  end

  it "resets card's repetitions count when answer quality is below 3" do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck, repetition_total: 12)
    card.manage_review_dates(14, true)
    expect(card.repetition_total).to eq 1
  end

  it "increases card's repetitions count when answer quality is above or equals 3" do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck, repetition_total: 12)
    card.manage_review_dates(2, true)
    expect(card.repetition_total).to eq 13
  end

  it "keeps changing card's e-factor after sucessful trials" do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck, repetition_total: 3)
    arr = []
    10.times do
      card.manage_review_dates(1, true)
      arr << card.efactor
    end
    expect(arr[0]).to be > 2.5
    expect(arr[7]).to be > arr[3]
  end

  it "doesn't change card's e-factor after some amount of failed attempts" do
    card = create(:card, original_text: "success", translated_text: "e", review_date: 3.days.from_now, user: user, deck: deck, repetition_total: 3)
    arr = []

    3.times do
      card.manage_review_dates(1, true)
      arr << card.efactor
    end

    5.times do
      card.manage_review_dates(1, false)
      arr << card.efactor
    end

    expect(arr[2]).to eq arr[7]
  end

end

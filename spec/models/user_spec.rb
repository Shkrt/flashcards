require 'rails_helper'

describe User, type: :model do
  let!(:user) { create(:user, email: 'test@te.st', password: 'qwerty') }

  it 'validates all fields' do
    user = build(:user, email: '', password: 'qwerty', password_confirmation: 'qwerty')
    user_1 = build(:user, email: '12345@to.pk', password: '', password_confirmation: 'qwerty')
    user_2 = build(:user, email: '12345@to.pk', password: 'qwerty', password_confirmation: '')
    user_3 = build(:user, email: '12345@to.pk', password: 'qwerty', password_confirmation: '111')
    user_4 = build(:user, email: '12345@to.pk', password: 'qwerty', password_confirmation: 'qwerty')
    expect(user).to be_invalid
    expect(user_1).to be_invalid
    expect(user_2).to be_invalid
    expect(user_3).to be_invalid
    expect(user_4).to be_valid
  end

  it ".take_card returns card belonging to review_expired scope" do
    deck = create(:deck, name: 'Test', user: user)
    card = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user, deck: deck)
    expect(user.take_card).to eq(card)
  end

  it ".take_card returns card for later repeating session when all today's cards already reviewed" do
    deck = create(:deck, name: 'Test', user: user)
    card = create(:card, original_text: 'A', translated_text: 'B', repeat_later: true, review_date: (3).days.from_now, user: user, deck: deck)
    user.review_complete
    expect(user.take_card).to eq(card)
  end

  it ".take_card takes card from current deck, when it's not empty" do
    deck_1 = create(:deck, name: 'Test', user: user, current: true)
    deck_2 = create(:deck, name: 'Test', user: user)
    card_1 = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user, deck: deck_1)
    card_2 = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user, deck: deck_2)
    expect(user.take_card).to eq(card_1)
  end

  it ".take_card takes any expired card, when current deck is empty" do
    deck_1 = create(:deck, name: 'Test', user: user, current: true)
    deck_2 = create(:deck, name: 'Test', user: user)
    card = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user, deck: deck_2)
    expect(user.take_card).to eq(card)
  end

  it ".take_card takes any expired card, when there is no deck marked as current" do
    deck_1 = create(:deck, name: 'Test', user: user)
    deck_2 = create(:deck, name: 'Test', user: user)
    card_1 = create(:card, original_text: 'A', translated_text: 'B', review_date: (3).days.from_now, user: user, deck: deck_1)
    card_2 = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user, deck: deck_2)
    expect(user.take_card).to eq(card_2)
  end

  it ".take_card doesn't touch other users' cards" do
    user_1 = create(:user, email: "exa@mp.le", password: "qwerty")
    deck = create(:deck, name: 'Test', user: user)
    deck_1 = create(:deck, name: 'Test', user: user_1)
    card = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user, deck: deck)
    card_1 = create(:card, original_text: 'A', translated_text: 'B', review_date: (-1).days.from_now, user: user_1, deck: deck_1)
    expect(user.take_card).to eq(card)
    expect(user_1.take_card).to eq(card_1)
  end

  it ".take_card returns nil when review and later repeating are both complete" do
    deck = create(:deck, name: 'Test', user: user)
    card = create(:card, original_text: 'A', translated_text: 'B', review_date: (3).days.from_now, user: user, deck: deck)
    expect(user.take_card).to be nil
  end

  it ".review_complete sets review_is_over to true when no more cards for review" do
    deck = create(:deck, name: 'Test', user: user)
    card = create(:card, original_text: 'A', translated_text: 'B', review_date: (3).days.from_now, user: user, deck: deck)
    user.review_complete
    expect(user.review_is_over).to be true
  end

  it ".review_complete doesn't change review_is_over when there are cards for review" do
    deck = create(:deck, name: 'Test', user: user)
    card = create(:card, original_text: 'A', translated_text: 'B', review_date: (-13).days.from_now, user: user, deck: deck)
    user.review_complete
    expect(user.review_is_over).not_to be true
  end
end

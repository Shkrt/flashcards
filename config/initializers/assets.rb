Rails.application.config.assets.version = "1.0"

Rails.application.config.assets.precompile += %w( bootstrap.min.css sticky-footer.css countdown.js )

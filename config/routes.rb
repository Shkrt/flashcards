Rails.application.routes.draw do
  root to: 'dashboard/cards#index'

  scope module: 'home' do
    get 'login' => 'user_sessions#new', :as => :login
    get 'oauth/callback' => 'oauths#callback'
    post 'oauth/callback' => 'oauths#callback'
    get 'oauth/:provider' => 'oauths#oauth', :as => :auth_at_provider
    resources :users, only: [:new, :create]
    resources :user_sessions, only: [:new, :index, :create]
  end

  scope module: 'dashboard' do
    post 'logout' => 'user_sessions#destroy', :as => :logout
    get '/cards/admin' => 'cards#admin'
    get '/remove_attachment' => 'cards#remove_attachment'
    post '/check' => 'cards#check'
    resources :cards, :decks, :users, :user_sessions
  end
end

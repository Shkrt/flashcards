set :output, File.expand_path("../../log/mailer_log", __FILE__)

every 1.day do
  runner "User.notify_cards_expired"
end

ip = "52.32.91.124"

role :app, ["ubuntu@#{ip}"]
role :web, ["ubuntu@#{ip}"]
role :db,  ["ubuntu@#{ip}"]

server ip, user: 'ubuntu', roles: %w{web app db}

set :stage, 'production'
set :rails_env, 'production'

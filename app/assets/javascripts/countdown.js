//= require jquery.countdown
var fifteenSeconds = new Date().getTime() + 15000;
$('#clock').countdown(fifteenSeconds).on('update.countdown', function(event) {
  var format = '%M:%S';
  $(this).html(event.strftime(format));
}).on('finish.countdown', function() {
  $(this).html('00:00').parent().addClass('disabled');
});

$(".edit_card")
.bind('ajax:beforeSend', function(event, xhr, settings){
  settings.data += "&elapsed=" + (15 - $('#clock').text().split(':')[1]);
});

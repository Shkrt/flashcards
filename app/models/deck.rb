class Deck < ActiveRecord::Base
  has_many :cards, dependent: :destroy
  belongs_to :user
  validates :name, presence: true
  validates_uniqueness_of :current, scope: :user_id, if: :current?,
    :message=>"Текущая колода уже выбрана"

end

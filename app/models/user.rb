class User < ActiveRecord::Base
  authenticates_with_sorcery! do |config|
    config.authentications_class = Authentication
  end

  has_many :cards, dependent: :destroy
  has_many :decks, dependent: :destroy
  has_many :authentications, dependent: :destroy
  accepts_nested_attributes_for :authentications
  validates :email, :password, presence: true, if: :new_user?
  validates :email, uniqueness: true
  validates :password, length: { minimum: 3 }, if: :new_user?
  validates :password, confirmation: true, if: :new_user?
  validates :password_confirmation, presence: true, if: :new_user?

  def take_card
    select_to_repeat if review_is_over
    card_set = get_cards.review_expired
    offset = rand(card_set.count)
    card_set.offset(offset).first
  end

  def self.notify_cards_expired
    Card.includes(:user).review_expired.select(:user_id).uniq
      .each { |r| CardsMailer.pending_card_notifications(r.user).deliver_now }
  end

  def review_complete
    update(review_is_over: true) unless get_cards.review_expired.any?
  end

  private

  def get_cards
    deck = decks.where(current: true).first
    deck && deck.cards.any? ? deck.cards : cards
  end

  def select_to_repeat
    repeat_later = cards.where(repeat_later: true)
    if repeat_later.any?
      repeat_later.each { |x| x.update(review_date: (-1).days.from_now, repeat_later: false) }
      update(review_is_over: false)
    end
  end

  def new_user?
    new_record?
  end
end

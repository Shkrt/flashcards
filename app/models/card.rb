class Card < ActiveRecord::Base
  attr_accessor :user_entered
  belongs_to :user
  belongs_to :deck
  has_attached_file :picture, styles: { medium: '360x360>', thumb: '100x100>' },
                              default_url: '/images/:style/missing.png'
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  validates :original_text, :translated_text, :review_date, :deck_id,
            presence: true
  validate :values_cannot_be_equal

  scope :review_expired, -> { where('review_date <=?', Time.now) }

  def check_translation(guess)
    if Levenshtein.distance(original_text.downcase, guess.downcase) <= 2
      msg = (I18n.t('flash.mistyped')).concat(': ').concat(original_text).
        concat(I18n.t('flash.you_entered')).concat(': ').concat(guess)
    end
    { correct: original_text.downcase == guess.downcase, message: msg }
  end

  def manage_review_dates(time, correct)
    updater = CardReviewUpdater.new(time, correct, efactor)
    quality = updater.quality
    repeat = quality < 4
    total_reps = quality < 3 ? 1 : repetition_total + 1
    interval = updater.get_interval(total_reps)

    update(review_date: interval.days.from_now, efactor: updater.efactor,
                             repeat_later: repeat, repetition_total: total_reps)
  end

  private

  def values_cannot_be_equal
    if translated_text.downcase.eql?(original_text.downcase)
      errors.add(:translated_text,
                 "Поля 'Оригинальный текст' и 'Переведенный текст' не должны совпадать")
    end
  end
end

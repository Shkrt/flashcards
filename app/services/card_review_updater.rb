class CardReviewUpdater
  attr_accessor :quality, :efactor
  INTERVALS = { 1 => 1, 2 => 6 }

  QUALITY = { 12..15 => 1, 10..11 => 2, 8..9 => 3, 2..7 => 4, 0..1 => 5 }

  def initialize(_time, correct_answer, old_efactor)
    @quality = correct_answer ? QUALITY.select { |x| x.include?(_time.to_i) }.values.first : 0
    @efactor = calculate_efactor(old_efactor)
  end

  def get_interval(repetitions)
    return 0 unless repetitions > 0
    CardReviewUpdater::INTERVALS[repetitions] || (@efactor * get_interval(repetitions - 1)).round
  end

  private

  def calculate_efactor(efactor)
    return efactor unless @quality >= 3
    new_efactor = efactor + (0.1 - (5 - @quality) * (0.08 + (5 - @quality) * 0.02))
    new_efactor >= 1.3 ? new_efactor : 1.3
  end
end

class ApplicationController < ActionController::Base
  before_filter :set_locale
  protect_from_forgery with: :exception
  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def render_404
    render file: "#{Rails.root}/public/404.html", status: 404
  end

  def set_locale
    locale = if current_user
               current_user.locale
             else
               session[:locale] = params[:locale] || session[:locale] ||
               http_accept_language.compatible_language_from(I18n.available_locales)
             end
    if locale && I18n.available_locales.include?(locale.to_sym)
      session[:locale] = I18n.locale = locale.to_sym
    end
  end
end

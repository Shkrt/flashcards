class Dashboard::CardsController < Dashboard::ApplicationController
  def index
    @card = current_user.take_card
    unless @card
      flash.keep[:info] = (t 'flash.no_cards')
      redirect_to cards_admin_path
    end
  end

  def edit
    @card = current_user.cards.find(params[:id])
  end

  def destroy
    @card = Card.find(params[:id])
    @card.destroy

    redirect_to cards_admin_path
  end

  def new
    @card = Card.new
  end

  def admin
    @cards = current_user.cards
  end

  def show
    redirect_to cards_admin_path
  end

  def check
    @card = Card.find(params[:card][:id])
    result = @card.check_translation(params[:card][:user_entered])
    setup_flash(result)
    @card.manage_review_dates(params[:elapsed], result[:correct])
    respond
  end

  def update
    @card = Card.find(params[:id])
    @card.update(card_params)

    redirect_to cards_admin_path
  end

  def create
    @card = Card.new(card_params)
    @card.user = current_user

    if @card.save
      redirect_to cards_admin_path
    else
      render 'new'
    end
  end

  def remove_attachment
    @card = Card.find(params[:id])
    @card.picture = nil
    @card.save
    redirect_to edit_card_path(@card)
  end

  private

  def card_params
    params.require(:card).permit(:original_text, :translated_text, :review_date, :user_id, :picture, :deck_id)
  end

  def respond
    current_user.review_complete
    @card = current_user.take_card
    respond_to do |format|
      if @card
        flash.discard
        format.js
      else
        @cards = current_user.cards
        format.js {render "admin"}
        flash.keep[:info] = (t 'flash.no_cards')
        flash.discard
      end
    end
  end

  def setup_flash(response)
    if response[:correct]
      flash[:success] = (t 'flash.answer_correct')
    else
      flash[:danger] = (t 'flash.answer_wrong')
      flash[:warning] = response[:message] if response[:message]
    end
  end
end

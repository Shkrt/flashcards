class Dashboard::DecksController < Dashboard::ApplicationController
  def new
    @deck = Deck.new
  end

  def index
    @decks = current_user.decks
  end

  def destroy
    @deck = Deck.find(params[:id])
    @deck.destroy

    redirect_to decks_path
  end

  def create
    @deck = Deck.new(deck_params)
    @deck.user = current_user

    if @deck.save
      redirect_to decks_path
    else
      render 'new'
    end
  end

  def update
    @deck = Deck.find(params[:id])
    if @deck.update(deck_params)
      redirect_to decks_path
    else
      flash[:danger] = @deck.errors[:current]
      redirect_to edit_deck_path(@deck)
    end
  end

  def edit
    @deck = Deck.find(params[:id])
  end

  private

  def deck_params
    params.require(:deck).permit(:name, :current, :user_id)
  end
end

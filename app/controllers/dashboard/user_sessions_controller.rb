class Dashboard::UserSessionsController < Dashboard::ApplicationController
  def destroy
    logout
    redirect_to(:root, success: (t 'flash.signout'))
  end
end

class Dashboard::ApplicationController < ApplicationController
  before_filter :require_login

  def not_authenticated
    redirect_to login_path
  end
end

class Dashboard::UsersController < Dashboard::ApplicationController
  def index
  end

  def edit
    @user = current_user
  end

  def update
    @user = User.find(params[:id])
    @user.update(user_params) ? (redirect_to users_path) : (render 'edit')
  end

  private

  def user_params
    params.require(:user).permit(:email,:password,:password_confirmation,:authentications_attributes, :locale)
  end
end

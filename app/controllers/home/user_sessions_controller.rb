class Home::UserSessionsController < Home::ApplicationController
  def new
    @user = User.new
  end

  def index
    redirect_to (:login)
  end

  def create
    if @user = login(params[:session][:email], params[:session][:password])
      redirect_back_or_to(:users, success: (t 'flash.signin'))
    else
      flash.now[:danger] = (t 'flash.unable_sign')
      render action: 'new'
    end
  end
end

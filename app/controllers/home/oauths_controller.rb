class Home::OauthsController < Home::ApplicationController
  def oauth
    login_at(user_params[:provider])
  end

  def callback
    provider = user_params[:provider]
    if @user = login_from(provider)
      redirect_to root_path, success: "Logged in from #{provider.titleize}!"
    else
      begin
        @user = create_from(provider)
        reset_session
        auto_login(@user)
        redirect_to root_path,
          success: "Logged in from #{provider.titleize}!"
      rescue
        redirect_to root_path,
          danger: "Failed to login from #{provider.titleize}!"
      end
    end
  end

  private
  def user_params
    params.permit(:provider, :code)
  end
end

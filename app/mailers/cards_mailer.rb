class CardsMailer < ApplicationMailer
  def pending_card_notifications(user)
    @user = user
    @url = Rails.application.config.action_mailer.default_url_options[:host]
    mail(to: @user.email, subject: default_i18n_subject(user: user.email))
  end
end

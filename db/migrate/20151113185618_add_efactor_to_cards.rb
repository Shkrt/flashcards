class AddEfactorToCards < ActiveRecord::Migration
  def change
    add_column :cards, :efactor, :float, default: 2.5
    add_column :cards, :repetition_total, :integer, default: 0
    remove_column :cards, :success_count
    remove_column :cards, :fail_count
  end
end

class CreateDecks < ActiveRecord::Migration
  def change
    create_table :decks do |t|
      t.string :name
      t.timestamps null: false
    end

    add_reference :cards, :deck, index: true, foreign_key: true
  end
end

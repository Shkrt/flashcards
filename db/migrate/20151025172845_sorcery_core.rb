class SorceryCore < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :crypted_password
      t.string :salt
      t.remove :password
    end
    change_column_null :users, :email, false
    add_index :users, :email, unique: true
  end
end

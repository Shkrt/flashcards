require 'nokogiri'
require 'open-uri'

link = 'http://www.101languages.net/icelandic/most-common-icelandic-words'
doc = Nokogiri::HTML(open(link))

doc.search('.row-hover tr').each do |l|
  original = l.css('.column-2').xpath('text()').text.squish
  translation = l.css('.column-3').text.downcase

  Card.create(original_text: original, translated_text: translation,
	        review_date: 3.days.from_now)
end
